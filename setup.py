# -*- coding: utf-8 -*-
from setuptools import setup
import sys
import pyder
from os import path

here = path.abspath(path.dirname(__file__))

try:
    import pypandoc

    long_description = pypandoc.convert("README.md", "rst")
except:
    with open(path.join(here, "README.md")) as f:
        long_description = f.read()

install_requires = [
    "click",
    "futures",
    "jupyter",
    "matplotlib",
    "networkx",
    "nose",
    "notebook",
    "numpy",
    "pandas",
    "six",
    "pytest",
    "jinja2",
]

setup(
    name=pyder.__title__,
    version=pyder.__version__,
    description=pyder.__summary__,
    long_description=long_description,
    license=pyder.__license__,
    url=pyder.__uri__,
    author=pyder.__author__,
    author_email=pyder.__email__,
    packages=["pyder"],
    entry_points={"console_scripts": ["pyder = pyder.cli:cli"]},
    install_requires=install_requires,
)
