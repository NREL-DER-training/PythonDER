__all__ = ['render', 'list']

import os

def list_variables(file_name):
    """List of variables in a template"""
    from jinja2 import Environment, FileSystemLoader, PackageLoader, meta

    if not os.path.isfile(file_name):
        path = os.path.dirname(os.path.abspath(__file__))
    else:
        path = os.path.dirname(os.path.abspath(file_name))
        file_name = os.path.basename(file_name)

    environment = Environment(
        autoescape=False,
        loader=FileSystemLoader(os.path.join(path, '.')),
        trim_blocks=False)

    template_source = environment.loader.get_source(environment, file_name)[0]
    parsed_content = environment.parse(template_source)

    return meta.find_undeclared_variables(parsed_content)

def list_templates():
    """List available templates"""
    mypath = os.path.dirname(os.path.abspath(__file__))
    mypath = os.path.join(mypath, '.')
    onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f)) and f.split('.')[-1]=='glm']
    return onlyfiles

def render_template(file_name, **kwargs):
    """Render template from `file_name`

    Example
    -------

        render('meter.dss',
            solar_meter_name='meter1',
            solar_meter_parent='node1')

    """

    from jinja2 import Environment, FileSystemLoader

    if not os.path.isfile(file_name):
        path = os.path.dirname(os.path.abspath(__file__))
    else:
        path = os.path.dirname(os.path.abspath(file_name))
        file_name = os.path.basename(file_name)

    environment = Environment(
        autoescape=False,
        loader=FileSystemLoader(os.path.join(path, '.')),
        trim_blocks=False)

    template = environment.get_template(file_name)

    output_from_parsed_template = template.render(**kwargs)

    return output_from_parsed_template
