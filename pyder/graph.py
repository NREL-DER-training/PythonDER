"""Module for graph operations"""

import os

import pandas as pd
import networkx as nx

def create_graph(file_name):
    """Creates feeder from file"""

    file_name = os.path.abspath(file_name)

    df = pd.read_csv(file_name, skiprows=3)

    G = nx.Graph()

    # Remove last row
    df = df[:-1]

    for i in range(0, len(df)):
        n1 = df.iloc[i]['node']
        x = df.iloc[i]['X_ft']
        y = df.iloc[i]['Y_ft']
        pos = "{},{}".format(x, y)
        G.add_node(n1, pos=pos, pin=True)

    for i in range(0, len(df)):
        n1 = df.iloc[i]['node']
        n2 = df.iloc[i]['ref_node']
        if n2 == 'None':
            continue
        G.add_edge(n1, n2)
        file_name

    return G

def get_voltage_map_for_graph(G, file_name, phase='A'):
    """Returns voltage at every node in the graph"""

    file_name = os.path.abspath(file_name)

    df = pd.read_csv(file_name, skiprows=1)
    df.index = df['node_name']

    voltage_map = {}

    for n in G.nodes():
        try:
            voltage_map[n] = df['volt{phase}_mag'.format(phase=phase)][n] 

        except KeyError:
            voltage_map[n] = 2400


    return voltage_map

def get_current_map_for_graph(G, file_name, phase='A', current_labels=False):
    """Returns voltage at every node in the graph"""

    file_name = os.path.abspath(file_name)

    df = pd.read_csv(file_name, skiprows=1)
    df.index = df['link_name']

    current_map = {}

    for node1, node2 in G.edges():
        n1 = node1.replace('node_', '')
        n2 = node2.replace('node_', '')
        line = 'ohl_{}_{}'.format(n1, n2)
        if line not in df['link_name']:
            line = 'ohl_{}_{}'.format(n2, n1)

        try:
            current_map[(node1, node2)] = df['curr{phase}_mag'.format(phase=phase)][line]
        except KeyError:
            current_map[(node1, node2)] = 0

    if current_labels:
        return current_map, current_map
    else:
        return current_map, False

def rescale_layout(pos, scale=1):

    for key in pos:

        pos[key] = pos[key]*scale

    return pos

