from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

import copy

import pandas as pd
import networkx as nx

from . import visualize
from . import templates
from .graph import create_graph, get_voltage_map_for_graph, get_current_map_for_graph, rescale_layout

def add_solar(**kwargs):
    """ Adds solar

    Parameters
    ==========

    input_data : string
        file_name with list of nodes to add solar to
    input_file_format : string
        method type (negative_load or solar)
    output_file_name : string
        file_name for template to be rendered into
    """

    input_data = kwargs.pop('input_data')
    input_file_format = kwargs.pop('input_file_format')
    output_file_name = kwargs.pop('output')

    df = pd.read_csv(os.path.abspath(input_data))
    write_file_content = templates.render_template('pre_solar.glm')
    write_file_content += "\n\n"

    if input_file_format == 'negative_load':
        for i in range(0, len(df)):
            row = df.loc[i]
            nominal_voltage = row['nominal_voltage']
            parent = row['parent']
            phases = row['phases']
            name = row['name']
            name = "{}_{}".format(name, phases)

            phase_list = list(phases.strip('N'))

            constant_power_phase = {}
            for phase in phase_list:
                constant_power_phase[phase] = row['constant_power_{}'.format(phase)]

            write_file_content += templates.render_template('meter.glm',
                                      nominal_voltage=nominal_voltage,
                                      name="meter_{}".format(name),
                                      parent=parent,
                                      phases=phases
                                     )
            write_file_content += templates.render_template('negative_load.glm',
                                      constant_power_phase=constant_power_phase,
                                      name="load_{}".format(name),
                                      parent="meter_{}".format(name),
                                      phases=phases,
                                      phase_list=phase_list
                                     )
            write_file_content += "\n\n"

    elif input_file_format == 'solar':
        for i in range(0, len(df)):
            row = df.loc[i]
            name = row['name']
            nominal_voltage = row['nominal_voltage']
            parent = row['parent']
            phases = row['phases']

            inverter_generator_status = row['INVERTER_generator_status']
            inverter_inverter_efficiency = row['INVERTER_inverter_efficiency']
            inverter_power_factor = row['INVERTER_power_factor']
            inverter_inverter_type = row['INVERTER_inverter_type']
            inverter_rated_power = row['INVERTER_rated_power']
            inverter_generator_mode = row['INVERTER_generator_mode']

            solar_generator_mode = row['SOLAR_generator_mode']
            solar_generator_status = row['SOLAR_generator_status']
            solar_panel_efficiency = row['SOLAR_efficiency']
            solar_panel_type = row['SOLAR_panel_type']
            solar_area = row['SOLAR_area']

            meter_name = "meter_{}".format(name)
            inverter_parent = meter_name
            inverter_name = "inverter_{}".format(name)
            solar_name = "solar_{}".format(name)
            solar_parent = inverter_name

            write_file_content += templates.render_template('meter.glm',
                                      nominal_voltage=nominal_voltage,
                                      name="meter_{}".format(name),
                                      parent=parent,
                                      phases=phases
                                     )
            write_file_content += templates.render_template('inverter.glm',
                                                            generator_status=inverter_generator_status,
                                                            inverter_efficiency=inverter_inverter_efficiency,
                                                            phases=phases,
                                                            power_factor=inverter_power_factor,
                                                            inverter_type=inverter_inverter_type,
                                                            parent=inverter_parent,
                                                            name=inverter_name,
                                                            rated_power=inverter_rated_power,
                                                            generator_mode=inverter_generator_mode
                                                           )

            write_file_content += templates.render_template('solar.glm',
                                                            generator_status=solar_generator_status,
                                                            efficiency=solar_panel_efficiency,
                                                            generator_mode=solar_generator_mode,
                                                            parent=solar_parent,
                                                            panel_type=solar_panel_type,
                                                            area=solar_area,
                                                            name=solar_name
                                                           )

            write_file_content += "\n\n"

    with open(output_file_name, 'w') as f:

        f.write(write_file_content)


def plot(**kwargs):
    """ plots feeder

    Parameters
    ==========

    input_data : string
        path to node.csv
    output : string
        path to graph.pdf
    voltage_data : string (optional)
        path to voltage_dump.csv
    current_data : string (optional)
        path to current_dump.csv
    phases : string (optional)
        string with phases, e.g. 'A', 'ABC'
        list of strings phases e.g. ['A', 'B', 'C']
    labels : list or bool
        False to turn off labels
        True to keep all labels
        list of labels to add only those labels to the plot
    current_labels : bool
        True for current values on lines
    """

    list_filename_phase = []

    input_file_name = kwargs.pop('input_data')
    output_file_name = kwargs.pop('output')
    voltage_file_name = kwargs.pop('voltage_data', None)
    current_file_name = kwargs.pop('current_data', None)
    phases = kwargs.pop('phases', 'A')
    debug = kwargs.pop('debug', False)
    labels = kwargs.pop('labels', False)
    xshift_labels = kwargs.pop('xshift_labels', None)
    yshift_labels = kwargs.pop('yshift_labels', None)
    current_labels = kwargs.pop('current_labels', False)
    voltage_labels = kwargs.pop('voltage_labels', False)
    font_size = kwargs.pop('font_size', 5)
    node_size = kwargs.pop('node_size', 100)
    nominal_voltage = kwargs.pop('nominal_voltage', 2401.7777)
    show_plot = not kwargs.pop('no_show_plot', False)
    force_ansi = kwargs.pop('force_ansi', False)
    vmin = kwargs.pop('vmin', 0.9)
    vmax = kwargs.pop('vmax', 1.1)

    node_color = None
    edge_color = None
    edge_labels = False
    maximum_current = None
    node_labels = False

    G = create_graph(input_file_name)
    pos = visualize.calculate_positions(G) # pos = {'node_1': (100, 100)}
    pos = rescale_layout(pos, 50)

    output_file_name = os.path.abspath(output_file_name) # output_file_name = '/path/to/filename'
    filename = os.path.basename(output_file_name) # filename = 'filename'

    if phases == 'all':

        if voltage_file_name is not None:
            maximum_node_color = {}
            for phase in ['A', 'B', 'C']:
                node_color = get_voltage_map_for_graph(G, voltage_file_name, phase=phase)
                for k, v in node_color.items():
                    try:
                        maximum_node_color[k] = max(maximum_node_color[k], v)
                    except KeyError:
                        maximum_node_color[k] = v

            node_color = maximum_node_color

            if voltage_labels:
                node_labels = True
            else:
                node_labels = False
            # node_color = {'node_1': 2400, ..., 'node123': 2398.02}

        if current_file_name is not None:
            # edge_color = {('node_1', 'node_2'): 300, ..., }
            maximum_edge_color = {}
            for phase in ['A', 'B', 'C']:
                edge_color, edge_labels = get_current_map_for_graph(G, current_file_name, phase=phase, current_labels=current_labels)
                for k, v in edge_color.items():
                    try:
                        maximum_edge_color[k] = max(maximum_edge_color[k], v)
                    except KeyError:
                        maximum_edge_color[k] = v

            edge_color = maximum_edge_color
            maximum_current = max([edge_color[key] for key in edge_color])*1.1

        filename_phase = filename
        folder = os.path.dirname(output_file_name)

        output_file_name_phase = os.path.abspath(os.path.join(folder, filename_phase))

        visualize.nx_plot(G, pos,
                write_file=output_file_name_phase,
                xshift_labels=xshift_labels,
                yshift_labels=yshift_labels,
                vmin=vmin,
                vmax=vmax,
                node_color=node_color,
                edge_color=edge_color,
                labels=labels,
                edge_labels=edge_labels,
                node_labels=node_labels,
                cmax=maximum_current,
                font_size=font_size,
                node_size=node_size,
                normalize_node_labels = nominal_voltage,
                show_plot = show_plot,
                force_ansi=force_ansi
                )

        list_filename_phase.append(filename_phase)


    else:

        # Convert 'ABC' to ['A', 'B', 'C']
        if isinstance(phases, str):
            phases = phases.strip('"').strip("'").strip('`')
            phases = list(phases)

        for phase in phases:

            if voltage_file_name is not None:
                node_color = get_voltage_map_for_graph(G, voltage_file_name, phase=phase)
                if voltage_labels:
                    node_labels = True
                else:
                    node_labels = False
                # node_color = {'node_1': 2400, ..., 'node123': 2398.02}

            if current_file_name is not None:
                edge_color, edge_labels = get_current_map_for_graph(G, current_file_name, phase=phase, current_labels=current_labels)
                # edge_color = {('node_1', 'node_2'): 300, ..., }
                maximum_current = max([edge_color[key] for key in edge_color])*1.1

            filename_phase = filename.split('.')[0] + '_phase{}.'.format(phase) + filename.split('.')[1]
            folder = os.path.dirname(output_file_name)

            output_file_name_phase = os.path.abspath(os.path.join(folder, filename_phase))

            visualize.nx_plot(G, pos,
                    write_file=output_file_name_phase,
                    xshift_labels=xshift_labels,
                    yshift_labels=yshift_labels,
                    vmin=vmin,
                    vmax=vmax,
                    node_color=node_color,
                    edge_color=edge_color,
                    labels=labels,
                    edge_labels=edge_labels,
                    node_labels=node_labels,
                    cmax=maximum_current,
                    font_size=font_size,
                    node_size=node_size,
                    normalize_node_labels = nominal_voltage,
                    show_plot = show_plot,
                    force_ansi = force_ansi,
                    )

            list_filename_phase.append(filename_phase)

    return list_filename_phase

