import matplotlib

# matplotlib.use('TkAgg')

import numpy as np
import networkx as nx
import pandas as pd
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np

def calculate_positions(G, layout=None, **kwargs):
    """
    Function to calculate positions from graph using layout

    if layout is not provided, networkx's `spring_layout` is used.
    For small graphs use layout=None, i.e. `spring_layout`
    For medium graphs (100 nodes) use layout='neato' [requires graphviz]
    For large graphs ( > 100 nodes ) use layout='fdp' [requires graphviz]
    For very large graphs, use layout='sfdp' [requires graphviz with gts]

    Parameters
    ------
    layout : string
        e.g. : 'neato', 'fdp', 'sfdp'
        default is None, which uses spring_layout
    args : string
        Arguments to pass to graphviz
        e.g. : '-Goverlap=False'

    Returns
    -------
    pos : dict
        keys with node names and values as (x,y) positions
        in the form of a tuple
    """

    if layout:
        pos = graphviz_layout(G, prog=layout, **kwargs) # {'1': (100.0, 100.0) }
    else:
        pos = nx.get_node_attributes(G, 'pos') # {'1': '100.0,100.0'}
        pos = {n: (float(pos[n].split(',')[0]), float(pos[n].split(',')[1])) for n in pos} # pos = {'1': (100.0, 100.0)}
        # Recalculate positions for the nodes that are not fixed
        pos = nx.spring_layout(G, pos=pos, fixed=G.nodes()) # {'1': (100.0, 100.0) }
    return pos


def nx_plot(G, pos,
        write_file=None,
        fig=None,
        ax=None,
        **kwargs
        ):
    """
    plot a networkx feeder

    Parameters
    ------
    write_file : string
        Path to store file
    fig : matplotlib figure
    ax : matplotlib axes
    figsize_X : float
    figsize_Y : float
    figsize_scale : float
    xshift_lables : float
    yshift_labels : float
    vmin : float
    vmax : float
    cmin : float
    cmax : float
    node_color : dict
        keys are the nodes
        values are floats
    edge_color : dict
    labels : bool, dict
        if True plot for all labels (default)
        list of labels to plot
        False to disable all labels
    node_labels : bool, dict
        if True plot all node_labels (default)
        keys are the nodes
        values are labels
    edge_labels : dict
        keys are the nodes forming edges as tuples
        values are labels
    font_size : float
        font size for all labels, nodes and edges


    Returns
    -------
    fig : matplotlib figure
    axes : matplotlib axes
    """

    # pop all arguments from kwargs
    factor = kwargs.pop('figsize_scale', 2)
    xshift = kwargs.pop('xshift_labels', 0)
    yshift= kwargs.pop('yshift_lables', -1500)
    labels = kwargs.pop('labels', None)
    node_color = kwargs.pop('node_color', None)
    edge_color = kwargs.pop('edge_color', None)
    cmin = kwargs.pop('cmin', 0)
    cmax = kwargs.pop('cmax', None)
    node_labels = kwargs.pop('node_labels', False)
    edge_labels = kwargs.pop('edge_labels', False)
    xnodelabelshift = kwargs.pop('xnodelabelshift', 0)
    ynodelabelshift= kwargs.pop('ynodelabelshift', 1500)
    xedgelabelshift = kwargs.pop('xedgelabelshift', 0)
    yedgelabelshift= kwargs.pop('yedgelabelshift', 0)
    font_size = kwargs.pop('font_size', 5)
    voltage_color_map = kwargs.pop('voltage_color_map', True)
    current_color_map = kwargs.pop('current_color_map', True)
    node_size = kwargs.pop('node_size', 100)
    normalize_node_labels = kwargs.pop('normalize_node_labels', 2401.777)
    vmin = kwargs.pop('vmin', 2300/normalize_node_labels)
    vmax = kwargs.pop('vmax', 2500/normalize_node_labels)
    show_plot = kwargs.pop('show_plot', True)

    matplotlib.rcParams.update({'font.size': font_size})

    force_ansi = kwargs.pop('force_ansi', False)

    if force_ansi:
        lower_limit = 0.25
        upper_limit = 0.75
    else:
        lower_limit = 0.35
        upper_limit = 0.65


    if fig is None and ax is None:
        X = kwargs.pop('figsize_X', 13*factor)
        Y = kwargs.pop('figsize_Y', 10*factor)
        fig, axs = plt.subplots(1, 1, figsize=(X, Y))
        ax = axs

    l_x  = []
    l_y = []
    for n, (x,y) in pos.items():
        l_x.append(x)
        l_y.append(y)

    if edge_color is not None:
        edge_color = [edge_color[n] for n in G.edges()]
    else:
        edge_color = 'black'
        current_color_map = False

    if node_color is not None:
        node_color = [node_color[n] / normalize_node_labels for n in G.nodes()]

        cdict = {'red':   ((0.0,  0.0, 0.0),   # From 0 to 0.25, we fade the red and green channels
                           (lower_limit, 0.5, 0.5),   # up a little, to make the blue a bit more grey

                           (lower_limit, 0.0, 0.0),   # From 0.25 to 0.75, we fade red from 0.5 to 1
                           (upper_limit, 1.0, 1.0),   # to fade from green to yellow

                           (1.0,  0.5, 0.5)),  # From 0.75 to 1.0, we bring the red down from 1
                                               # to 0.5, to go from bright to dark red

                 'green': ((0.0,  0.0, 0.0),   # From 0 to 0.25, we fade the red and green channels
                           (lower_limit, 0.6, 0.6),   # up a little, to make the blue a bit more grey

                           (lower_limit, 1.0, 1.0),   # Green is 1 from 0.25 to 0.75 (we add red
                           (upper_limit, 1.0, 1.0),   # to turn it from green to yellow)

                           (upper_limit, 0.0, 0.0),   # No green needed in the red upper quarter
                           (1.0,  0.0, 0.0)),

                 'blue':  ((0.0,  0.9, 0.9),   # Keep blue at 0.9 from 0 to 0.25, and adjust its
                           (lower_limit, 0.9, 0.9),   # tone using the green and red channels

                           (lower_limit, 0.0, 0.0),   # No blue needed above 0.25
                           (1.0,  0.0, 0.0))

                     }

        cmap = colors.LinearSegmentedColormap('BuGnYlRd',cdict)

    else:
        node_color = 'r'
        voltage_color_map = False
        cmap = plt.cm.get_cmap('RdYlBu')

    nodes = nx.draw_networkx_nodes(G, pos=pos,
            ax=ax,
            cmap=cmap,
            node_size=node_size,
            vmin=vmin,
            vmax=vmax,
            node_color=node_color)

    edges = nx.draw_networkx_edges(G, pos=pos,
            ax=ax,
            edge_cmap=plt.cm.get_cmap('hot'),
            edge_color=edge_color,
            edge_vmin=cmin,
            edge_vmax=cmax)

    if edge_labels is not False:
        label_pos=dict((n,(x+xedgelabelshift,y+yedgelabelshift)) for n,(x,y) in pos.items())
        edge_labels = {(n1, n2): round(v, 2) for (n1, n2), v in edge_labels.items()}
        edge_labels=nx.draw_networkx_edge_labels(G,
                pos=label_pos,
                edge_labels=edge_labels,
                font_size=font_size,
                rotate=False,
                )


    if labels != False:
        label_pos=dict((n,(x+xshift,y+yshift)) for n,(x,y) in pos.items())
        if labels == True:
            labels = None # plots all labels
        nx.draw_networkx_labels(G,
                label_pos,
                font_size=font_size,
                labels=labels)

    if node_labels != False:
        label_pos=dict((n,(x+xnodelabelshift,y+ynodelabelshift)) for n,(x,y) in pos.items())
        if node_labels == True:
            node_labels = {n: round(node_color[i], 2) for i, n in enumerate(G.nodes())}
        nx.draw_networkx_labels(G,
                label_pos,
                font_size=font_size,
                labels=node_labels)

    if voltage_color_map:
        # create an axes on the right side of ax. The width of cax will be 5%
        # of ax and the padding between cax and ax will be fixed at 0.05 inch.
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("left", size="5%", pad=0.05)
        cb = plt.colorbar(nodes, cax=cax)
        cax.yaxis.set_label_position('left')
        cax.yaxis.set_ticks_position('left')
        cb.set_label('Voltage (V)')

    if current_color_map:
        # create an axes on the right side of ax. The width of cax will be 5%
        # of ax and the padding between cax and ax will be fixed at 0.05 inch.
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cb = plt.colorbar(edges, cax=cax)
        cax.yaxis.set_label_position('right')
        cax.yaxis.set_ticks_position('right')
        cb.set_label('Current (A)')

    ax.axis('off')

    if write_file:
        plt.savefig(write_file)

    if show_plot:
        plt.show()
