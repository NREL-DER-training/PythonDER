#!/usr/bin/env python
"""cli module."""
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import click
import traceback
from . import pyder
from . import version

@click.group()
@click.version_option(version.__version__, '--version')
def cli():
    pass

@cli.command()
@click.option('--input-data', help='Path to .csv file', prompt="Path to feeder.csv")
@click.option('--output', default='plot.pdf', help='Path to output file')
@click.option('--voltage-data', default=None, help='Path to voltage dump file from GridLAB-D')
@click.option('--current-data', default=None, help='Path to current dump file from GridLAB-D')
@click.option('--labels', is_flag=True, help='Flag to turn node name labels on')
@click.option('--voltage-labels', is_flag=True, help='Flag to turn voltage labels on')
@click.option('--current-labels', is_flag=True, help='Flag to turn current labels on')
@click.option('--phases', default='all', help='String or list of phases. (default = "all"), e.g. --phases=AB or --phases=ABC')
@click.option('--xshift-labels', type=float, default=0, help='Shift labels in the x direction')
@click.option('--yshift-labels', type=float, default=0, help='Shift labels in the y direction')
@click.option('--font-size', default=5, type=float, help='Set font size (default=5) e.g. --font-size=20')
@click.option('--node-size', default=100, type=float, help='Node size (default=100) e.g. --node-size=500')
# @click.option('--debug', default=False, help='')
@click.option('--nominal-voltage', default=2401.777, type=float, help='nominal_voltage (default=2401.777)')
@click.option('--no-show-plot', default=False, is_flag=True, help='Suppress show plot. Show plot is an experimental feature. If it causes issues, use --no-show-plot to suppress')
@click.option('--force-ansi', default=False, is_flag=True, help='Force ANSI limits on voltage color map')
@click.option('--vmin', default=0.9, type=float, help='voltage minimum value (default = 0.9)')
@click.option('--vmax', default=1.1, type=float, help='voltage maximum value (default = 1.1)')
def plot(**kwargs):
    """ Plot """

    list_filenames = pyder.plot(**kwargs)

    for f in list_filenames:
        click.secho("Created output file ", nl=False)
        click.secho("{}".format(f), fg='green')

@cli.command(name='add-solar')
@click.option('--input-data', help='Path to .csv file', prompt="Path to solar.csv")
@click.option('--is-negative-load', 'input_file_format', help='Flag to negative load file format (method1) for --input-data file', flag_value='negative_load', default=True)
@click.option('--is-solar', 'input_file_format', help='Flag to inverter solar file format (method2) for --input-data file', flag_value='solar')
@click.option('--output', help='Path to output .glm file', prompt="Path to output.glm file")
def add_solar(**kwargs):
    """ Add solar """

    pyder.add_solar(**kwargs)

if __name__ == '__main__':
    cli()


