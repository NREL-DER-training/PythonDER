# pyder

Python for DER

### INSTALL

**With virtual environments (RECOMMENDED)**

    git clone https://gitlab.com/NREL-DER-training/PythonDER.git
    conda create -n pyder-env python=2
    source activate pyder-env
    conda install matplotlib
    conda install pandas
    pip install -e .

**Without virtual environments**

    git clone https://gitlab.com/NREL-DER-training/PythonDER.git
    conda install matplotlib
    conda install pandas
    pip install -e .


### CLI


`pyder --help`

    Usage: pyder [OPTIONS] COMMAND [ARGS]...

    Options:
      --version  Show the version and exit.
      --help     Show this message and exit.

    Commands:
      add-solar  Add solar
      plot       Plot

`pyder plot --help`

    Usage: pyder plot [OPTIONS]

      Plot

    Options:
      --input-data TEXT      Path to .csv file
      --output TEXT          Path to output file
      --voltage-data TEXT    Path to voltage dump file
      --current-data TEXT    Path to current dump file
      --labels               Flag to turn labels on
      --current-labels       Flag to turn labels on
      --phases TEXT          String or list of phases
      --xshift-labels FLOAT
      --yshift-labels FLOAT
      --font-size FLOAT      Font size
      --debug TEXT
      --help                 Show this message and exit.

`pyder add-solar --help`

    Usage: pyder add-solar [OPTIONS]

      Add solar

    Options:
      --input-data TEXT   Path to .csv file
      --is-negative-load  input file format
      --is-solar          input file format
      --output TEXT       Path to output .glm file
      --help              Show this message and exit.

### Example

**plot**

Create voltage and current plot.

Example 1

    pyder plot --input-data ./data/test-nodes.csv --output graph.pdf --labels

Example 2

    pyder plot --input-data ./data/test-nodes.csv --output graph.png --voltage-data ./output/ieee123_Vdump.csv --phases='ABC' --current-data ./ieee123_Idump.csv

Example 3

    pyder plot --input-data ./data/test-nodes.csv --output graph.pdf --voltage-data ./output/ieee123_Vdump.csv --phases='A' --current-data ./ieee123_Idump.csv --current-labels --labels

**add-solar**

Add solar to the feeder (method1)

    pyder add-solar --input-data ./data/solar_method1.csv --output output.glm --is-negative-load

Add solar to the feeder (method2)

    pyder add-solar --input-data ./data/solar_method2.csv --output output.glm --is-solar

**Note**

Use backslash (`\`) on Windows

Use forward slash (`/`) on OSX